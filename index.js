import express, { json, urlencoded } from "express";

import { config } from "dotenv";
import { connectDb } from "./src/connectdb/db.js";
import { apiVersion, port, staticFolder } from "./src/config/config.js";
import errorHandler from "./src/middleware/errorHandler.js";
import cors from "cors";
import apiRouter from "./src/routes/index.js";
import limiter from "./src/middleware/rateLimiter.js";
let expressApp = express();
config();

let corseOption = {
  origin: [
    "http://localhost:8002",
    "http://localhost:8001",
    "https://project-dw.onrender.com",
  ],
};
// app.use(cors());
expressApp.use(limiter);
expressApp.use(cors());
expressApp.use(json());
expressApp.use(urlencoded({ extended: true }));

expressApp.use(`${apiVersion}`, apiRouter);

expressApp.use(express.static(staticFolder));
// expressApp.use("/aaa", firstRouter);

expressApp.use(errorHandler);

connectDb();
console.log(port);

expressApp.listen(port, () => {
  console.log("the port is listening at 8000");
});

// admin user
