import { RegexPasswordValidation } from "../regexTextValidation/regexTextValidation.js";
import { AdminRegister, TokenModel } from "../SchemasModle/model.js";
import { comparePassword, hashPassword } from "../utils/hassFunction.js";
import { generateToken } from "../utils/token.js";

export let createUser = async (req, res) => {
  //password hashing
  let data = { ...req.body };
  try {
    if (!RegexPasswordValidation(data.password)) {
      const error = new Error("Password is not valid.");
      error.statusCode = "400";
      throw error;
    }
    data.password = await hashPassword(data.password);
    let result = await AdminRegister.create(data);
    let jsonSuccessMessage = {
      status: "success",
      message: "User created successfully.",
      data: result,
    };
    res.status(201).json(jsonSuccessMessage);
  } catch (error) {
    let jsonFailureMessage = {
      status: "error",
      message: error.message,
    };
    if (error.statusCode) {
      res.status(error.statusCode).json(jsonFailureMessage);
    }
    res.json(jsonFailureMessage);
  }
};

export let loginAdminUser = async (req, res) => {
  //process
  //1)check if email exist
  //2)if email exist check if hashpassword match
  //3) if matched generate token and and save token
  let { email, password } = req.body;

  try {
    let user = await AdminRegister.findOne({ email: email }); //findOne gives null(if not found) or object

    if (user === null) {
      throw new Error("Please enter valid email or password."); //Please enter valid email or password. message is use so that no one can hack
    } else {
      let hashPassword = user.password;
      let isMatchedPassword = await comparePassword(password, hashPassword);
      let secretKey = process.env.SECRET_KEY || "secretkey";
      let expiresIn = process.env.EXPIRES_IN || "365d";

      if (isMatchedPassword) {
        //add token in UserModel

        let token = await generateToken(
          { userId: user._id },
          secretKey,
          expiresIn
        );

        let data = { token: token };

        let result = await TokenModel.create(data);
        res.status(201).json({
          status: "success",
          message: "Login Successfully.",
          token: result.token,
        });
      } else {
        let error = new Error("Please enter valid email or password.");
        error.statusCode = 400;
        throw error;
      }
    }
  } catch (error) {
    let statusCode = error.statusCode || 400;
    res.status(statusCode).json({
      status: "failed",
      message: error.message,
    });
  }
};

export let logoutAdminUser = async (req, res) => {
  let { tokenId } = req.token;
  try {
    await TokenModel.findByIdAndDelete(tokenId);

    res.status(203).json({
      status: "success",
      message: "User logout successfully",
    });
  } catch (error) {
    let statusCode = error.statusCode || 400;
    res.status(statusCode).json({
      status: "error",
      message: error.message,
    });
  }
};

export let updateAdminPassword = async (req, res) => {
  let { userId } = req.info;

  let { password } = req.body;

  try {
    let hashPassword1 = await hashPassword(password);
    let data = {
      password: hashPassword1,
    };
    let result = await AdminRegister.findByIdAndUpdate(userId, data);
    //since result gives password thus we have to remove password so use find
    // console.log(result._doc); //it gives the real docs
    delete result._doc.password; //IMP******************* we can not do delete result.password

    res.status(201).json({
      status: "success",
      message: "Password updated successfully.",
      data: result,
    });
  } catch (error) {
    let statusCode = error.statusCode || 400;
    res.status(statusCode).json({
      status: "error",
      message: error.message,
    });
  }
};

export let updateAdminProfile = async (req, res) => {
  let { userId } = req.info;

  let data = req.body;

  //delete password if password is send

  delete data.password;
  delete data.email;

  try {
    let result = await AdminRegister.findByIdAndUpdate(userId, data, {
      new: true,
      runValidators: true,
    });
    delete result._doc.password;
    delete result._doc.email;

    res.status(201).json({
      status: "success",
      message: "Profile updated successfully.",
      data: result,
    });
  } catch (error) {
    let statusCode = error.statusCode || 400;
    res.status(statusCode).json({
      status: "error",
      message: error.message,
    });
  }
};




import { TokenModel } from "../SchemasModle/model.js";
import { verifyToken } from "../utils/token.js";

//and add info in request if token is valid
let isValidToken = async (req, res, next) => {
  // token is always passed form Bearer Token
  //and token is always get from req.headers at authorization

  //authorization is in format Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2NzI0MDExODMsImV4cCI6MTY3MjQwMTMwM30.2Rq328j42u7QnnDOJJByrzvESgNlZ1OCI3HIoZiffEo

  //process
  //   1)check weather the authorization starts with Bearer
  //   2) if yes check the token is valid
  //   3)if valid send req.info = token detail data  (you can named info to any)

  let { authorization = "" } = req.headers;
  let isStartsWithBearer = authorization.startsWith("Bearer");
  let token = authorization.split(" ")[1];

  if (isStartsWithBearer && token) {
    let secretKey = process.env.SECRET_KEY || "secretkey";
    try {
      let result = await TokenModel.findOne({ token: token });

      if (result === null) {
        let error = new Error("Token not valid.");
        error.statusCode = 401;
        throw error;
      } else {
        req.token = {
          token: token,
          tokenId: result._id,
        };

        let info = await verifyToken(token, secretKey);
        req.info = info;
        // here you can do req.user = info

        next();
      }
    } catch (error) {
      let statusCode = error.statusCode || 401;
      res.status(statusCode).json({
        status: "error",
        message: error.message,
      });
    }
  } else {
    res.status(401).json({
      status: "failure",
      message: "Token is not valid",
    });
  }
};

export default isValidToken;
export default isValidToken;
export default isValidToken;





