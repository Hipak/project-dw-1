//product create (only admin and superadmin)
//product read() (no login required)
//product read specific (no login required)
//product update(only admin and superadmin)
//product delete (only admin and superadmin)

import { Router } from "express";
import { productController } from "../controllers/index.js";
import { sortFilterPagination } from "../middleware/sortSelectPage.js";

export const productRouter = Router();

productRouter
  .route("/")
  .post(
    // isValidToken,
    // isAuthorized(["admin", "superAdmin"]),
    productController.createProduct
  )
  .get(productController.readAllProduct, sortFilterPagination)
  // .get(readAllProduct)
  .delete();
productRouter
  .route("/:id")

  .patch(
    // isValidToken,
    // isAuthorized(["admin", "superAdmin"]),
    productController.updateProduct
  )
  .get(productController.readSpecificProduct)
  .delete(
    // isValidToken,
    // isAuthorized(["admin", "superAdmin"]),
    productController.deleteSpecificProduct
  );

export default productRouter;
