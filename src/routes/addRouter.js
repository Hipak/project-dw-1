import { Router } from "express";
import { addressController } from "../controllers/index.js";
import { sortFilterPagination } from "../middleware/sortSelectPage.js";
export const addRouter = Router();

addRouter
  .route("/")
  .post(addressController.createAddress)
  .get(addressController.readAllAddress, sortFilterPagination);
addRouter
  .route("/:id")
  .patch(addressController.updateAddress)
  .get(addressController.readSpecificAddress)
  .delete(addressController.deleteSpecificAddress);

export default addRouter;
