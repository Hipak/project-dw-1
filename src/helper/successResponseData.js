import { HttpStatus } from "../constant/constant.js";

const successResponseData = ({
  res,
  data = null,
  message = "",
  statusCode = HttpStatus.OK,
}) => {
  res.status(statusCode).json({
    success: true,
    data,
    message,
  });
};

export default successResponseData;
