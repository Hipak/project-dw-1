import { Address } from "../schemasModle/model.js";

export const createAddressService = async ({ data }) => Address.create(data);
export const updateAddressService = async ({ id, data }) =>
  Address.findByIdAndUpdate(id, data, {
    new: true,
    runValidators: true,
  });
export const readSpecificAddressService = async ({ id }) =>
  Address.findById(id);
export const readAllAddressService = async ({
  find = {},
  sort = "",
  limit = "",
  skip = "",
  select = "",
}) => Address.find(find).sort(sort).limit(limit).skip(skip).select(select);
export const deleteSpecificAddressService = async ({ id }) =>
  Address.findByIdAndDelete(id);
