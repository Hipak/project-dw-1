import { Schema } from "mongoose";

// schema means defining content of document
const productSchema = Schema(
  {
    name: {
      type: String,
      required: [true, "field is required"],
      trim: true,
    },
    quantity: {
      type: Number,
      required: [true, "field is required"],
      trim: true,
    },
    price: {
      type: Number,
      required: [true, "field is required"],
      trim: true,
    },
    featured: {
      type: Boolean,
      default: false,
      trim: true,
    },
    productImage: {
      type: String,
      trim: true,
      required: true,
    },
    manufactureDate: {
      type: Date,
      default: new Date(),
      trim: true,
    },
    company: {
      type: String,
      trim: true,
      enum: {
        values: ["apple", "samsung", "dell", "mi"],
        message: (enumValue) => {
          return `${enumValue.value} is not valid enum`;
        },
      },
    },
  },
  {
    timestamps: true,
  }
);

export default productSchema;
