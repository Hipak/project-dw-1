import { Schema } from "mongoose";

export let reviewSchema = Schema(
  {
    productRating: {
      type: Number,
      trim: true,
      required: [true, "name field is required"],
    },
    comment: {
      type: String,
      trim: true,
      required: [true, "name field is required"],
    },
    productId: {
      type: Schema.ObjectId,
      ref: "Product",
      // not during you cn use populate("productId") to populate productId info
      //   always use collection Name with singular  instead of ModleName
    },
  },

  {
    timestamps: true,
  }
);
