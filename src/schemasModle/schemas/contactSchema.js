import { Schema } from "mongoose";

export let contactSchema = Schema(
  {
    fullName: {
      type: String,
      trim: true,
      required: [true, "fullName field is required"],
    },
    address: {
      type: String,
      trim: true,
      required: [true, "address field is required"],
    },
    phoneNumber: {
      type: String,
      trim: true,
      required: [true, "phoneNumber field is required"],
    },
    email: {
      type: String,
      trim: true,
      required: [true, "email field is required"],
    },
  },

  {
    timestamps: true,
  }
);
