import { Schema } from "mongoose";

export const tokenSchema = Schema({
  token: {
    type: String,
    required: [true, "Please enter your token"],
    trim: true,
  },
});
