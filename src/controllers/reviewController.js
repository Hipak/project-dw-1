import { HttpStatus } from "../constant/constant.js";
import successResponseData from "../helper/successResponseData.js";
import tryCatchWrapper from "../middleware/tryCatchWrapper.js";
import { reviewService } from "../services/index.js";

export let createReview = tryCatchWrapper(async (req, res) => {
  let body = { ...req.body };
  let data = await reviewService.createReviewService({ data: body });

  successResponseData({
    res,
    message: "Review created successfully.",
    statusCode: HttpStatus.CREATED,
    data,
  });
});
export let updateReview = tryCatchWrapper(async (req, res) => {
  let body = { ...req.body };
  let id = req.params.id;

  let data = await reviewService.updateReviewService({ data: body, id });

  successResponseData({
    res,
    message: "Review updated successfully.",
    statusCode: HttpStatus.CREATED,
    data,
  });
});

export let readSpecificReview = tryCatchWrapper(async (req, res) => {
  let id = req.params.id;

  let data = await reviewService.readSpecificReviewService({ id });

  successResponseData({
    res,
    message: "Read  review successfully.",
    statusCode: HttpStatus.OK,
    data,
  });
});

export let readAllReview = tryCatchWrapper(async (req, res, next) => {
  let find = {};

  if (req.query.comment) {
    find.comment = { $regex: req.query.comment, $options: "i" };
  }

  if (req.query.productRating) {
    find.productRating = req.query.productRating;
  }

  if (req.query.productId) {
    find.productId = req.query.productId;
  }
  req.find = find;
  req.service = reviewService.readAllReviewService;

  next();
});

export let deleteSpecificReview = tryCatchWrapper(async (req, res) => {
  let id = req.params.id;
  let data = await reviewService.deleteSpecificReviewService({ id });

  successResponseData({
    res,
    message: "Review delete successfully.",
    statusCode: HttpStatus.OK,
    data,
  });
});

// import tryCatchWrapper from "../middleware/tryCatchWrapper.js";
// import { Review } from "../schemasModle/model.js";
// import {
//   getExactPageData,
//   selectField,
//   sortingFun,
// } from "../utils/pageSortFilter.js";

// export let createReview = tryCatchWrapper(async (req, res) => {
//   let data = { ...req.body };
//   let result = await Review.create(data);

//   let successJson = {
//     status: "success",
//     message: "Review created successfully.",
//     result: result,
//   };
//   res.status(201).json(successJson);
// });
// export let updateReview = tryCatchWrapper(async (req, res) => {
//   let data = { ...req.body };
//   let id = req.params.id;

//   let result = await Review.findByIdAndUpdate(id, data, {
//     new: true,
//     runValidators: true,
//   });

//   let successJson = {
//     status: "success",
//     message: "Review updated successfully.",
//     result: result,
//   };
//   res.status(201).json(successJson);
// });

// export let readSpecificReview = tryCatchWrapper(async (req, res) => {
//   let id = req.params.id;
//   try {
//     let result = await Review.findById(id).populate("productId");
//     let successJson = {
//       status: "success",
//       message: "Read  review successfully.",
//       data: result,
//     };

//     res.status(200).json(successJson);
//   } catch (error) {
//     let errorJson = {
//       status: "failure",
//       message: error.message,
//     };

//     let newStatus = error.statusCode || 400;
//     res.status(newStatus).json(errorJson);
//   }
// });

// export let readAllReview = tryCatchWrapper(async (req, res) => {
//   // for sorting
//   let sort = sortingFun(req.query._sort);
//   //for pagination
//   let { limit, skip } = getExactPageData(
//     req.query._brake,
//     req.query._page,
//     req.query._showAllData
//   );

//   let select = selectField(req.query._select);

//   //for searching
//   let find = {};

//   if (req.query.comment) {
//     find.comment = { $regex: req.query.comment, $options: "i" };
//   }

//   if (req.query.productRating) {
//     find.productRating = req.query.productRating;
//   }

//   if (req.query.productId) {
//     find.productId = req.query.productId;
//   }

//   let results = await Review.find(find)
//     .sort(sort)
//     .limit(limit)
//     .skip(skip)
//     .select(select)
//     .populate("productId");
//   console.log("****", results);
//   let totalDataInAPage = results.length;
//   let totalResults = await Review.find({});
//   let totalDataInWholePage = totalResults.length;
//   let currentPage = req.query._page || 1;

//   let successJson = {
//     status: "success",
//     message: "Read all review successfully.",
//     data: {
//       results,
//       totalDataInAPage,
//       totalDataInWholePage,
//       currentPage,
//     },
//   };

//   res.status(200).json(successJson);
// });

// export let deleteSpecificReview = tryCatchWrapper(async (req, res) => {
//   let id = req.params.id;
//   let result = await Review.findByIdAndDelete(id);
//   let successJson = {
//     status: "success",
//     message: "Review delete successfully.",
//     data: result,
//   };

//   res.status(200).json(successJson);
// });
